#include "trianglevoice.h"

TriangleVoice::TriangleVoice()
{
}

jack_default_audio_sample_t TriangleVoice::generate(jack_default_audio_sample_t ramp)
{
    return (ramp < 0.0 ?
        (ramp < -0.5 ? -2.0 * ramp - 2.0 : 2.0 * ramp) :
        (ramp >  0.5 ? -2.0 * ramp + 2.0 : 2.0 * ramp)
    );
}
