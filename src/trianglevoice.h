#ifndef TRIANGLEVOICE_H
#define TRIANGLEVOICE_H

#include "voice.h"

class TriangleVoice: public Voice
{
public:
    TriangleVoice();

    jack_default_audio_sample_t generate(jack_default_audio_sample_t ramp);
};

#endif // TRIANGLEVOICE_H
