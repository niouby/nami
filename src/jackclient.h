#ifndef JACKCLIENT_H
#define JACKCLIENT_H

#include <iostream>
#include <jack/jack.h>
#include <jack/midiport.h>

#include "buffer.h"

using namespace std;

class JackClient
{
private:
    // For JACK.
    jack_client_t *client;
    jack_port_t   *midiInputPort;
    jack_port_t   *leftAudioOutputPort;
    jack_port_t   *rightAudioOutputPort;

    Buffer *buffer;

public:
    JackClient(Buffer &buffer);

    // JACK's callbacks.
    static int process(jack_nframes_t nframes, void *arg);
    static void shutdown(void *arg);
};

#endif // JACKCLIENT_H
