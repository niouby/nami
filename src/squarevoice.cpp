#include "squarevoice.h"

SquareVoice::SquareVoice()
{
}

jack_default_audio_sample_t SquareVoice::generate(jack_default_audio_sample_t ramp)
{
    return (ramp < 0.0 ? -1.0 : 1.0);
}
