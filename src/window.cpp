#include "window.h"

// TO REMOVE: multiple definitions.
#include "constants.h"


void Window::setSineVoice(bool) {
    this->oscillatorTypeDisplay->setText("Sine");
    this->oscillatorPreview->setPixmap(QPixmap(PATH+"/images/sine_preview.png"));
}
void Window::setTriangleVoice(bool) {
    this->oscillatorTypeDisplay->setText("Triangle");
    this->oscillatorPreview->setPixmap(QPixmap(PATH+"/images/triangle_preview.png"));
}
void Window::setSquareVoice(bool) {
    this->oscillatorTypeDisplay->setText("Square");
    this->oscillatorPreview->setPixmap(QPixmap(PATH+"/images/square_preview.png"));
}
void Window::setSawVoice(bool) {
    this->oscillatorTypeDisplay->setText("Saw");
    this->oscillatorPreview->setPixmap(QPixmap(PATH+"/images/saw_preview.png"));
}

void Window::setLowFilter(bool) {
    this->filterTypeDisplay->setText("Low Pass");
    this->filterPreview->setPixmap(QPixmap(PATH+"/images/low_preview.png"));
}
void Window::setHighFilter(bool) {
    this->filterTypeDisplay->setText("High Pass");
    this->filterPreview->setPixmap(QPixmap(PATH+"/images/high_preview.png"));
}
void Window::setBandFilter(bool) {
    this->filterTypeDisplay->setText("Band Pass");
    this->filterPreview->setPixmap(QPixmap(PATH+"/images/band_preview.png"));
}
void Window::setNotchFilter(bool) {
    this->filterTypeDisplay->setText("Notch");
    this->filterPreview->setPixmap(QPixmap(PATH+"/images/notch_preview.png"));
}

Window::Window(Oscillator* o, Filter *filter) : QWidget()
{
    this->osci = o;
    this->filter = filter;
    this->setWindowTitle("Nami - Virtual Synthetizer");
    this->setProperty("class","mainWindow");

    // ----------------------
    // init window style
    // ----------------------
    //load the default layout with the stylesheet
    QFile styleFile( PATH + "style.qss" );
    styleFile.open( QFile::ReadOnly );
    QString style( styleFile.readAll() );
    style.replace("@PATH", PATH);
    style.replace("@LIGHTRED",   "rgb(238,  58,  58)");
    style.replace("@DARKRED",    "rgb( 65,   6,   6)");
    style.replace("@LIGHTBLUE",  "rgb(138, 212, 189)");
    style.replace("@DARKBLUE",   "rgb( 23,  64,  53)");
    style.replace("@NAVYBLUE",   "rgb( 11,  30,  25)");
    style.replace("@LIGHTGREEN", "rgb( 13, 200,  29)");
    style.replace("@DARKGREEN",  "rgb(  5,  73,   4)");
    style.replace("@LIGHTBRUN",  "rgb( 52,  38,  31)");
    style.replace("@DARKBRUN",   "rgb( 23,  17,  14)");
    style.replace("@LIGHTPINK",  "rgb(248, 199, 173)");
    this->setStyleSheet( style );

    QGridLayout *windowLayout = new QGridLayout;
    windowLayout->setSpacing(8);
    this->logoFrame = new QFrame();
    this->volumeFrame = new QFrame();
    this->oscillatorFrame = new QFrame();
    this->filterFrame = new QFrame();
    this->envelopeFrame = new QFrame();
    this->menuFrame = new QFrame();

    windowLayout->addWidget(logoFrame, 1,1);
    windowLayout->addWidget(volumeFrame, 4,1,1,2);
    windowLayout->addWidget(oscillatorFrame, 2,1,1,2);
    windowLayout->addWidget(filterFrame, 3,1,1,2);
    windowLayout->addWidget(envelopeFrame,2,3,3,1);
    windowLayout->addWidget(menuFrame, 1,2,1,2);

    this->envelopeFrame->setEnabled(false);
    this->menuFrame->setEnabled(false);

    this->logoFrame->setProperty("class","logo");
    this->volumeFrame->setProperty("class", "volume");
    this->oscillatorFrame->setProperty("class","oscillator");
    this->filterFrame->setProperty("class","filter");
    this->envelopeFrame->setProperty("class","envelope");
    this->menuFrame->setProperty("class","menu");

    // ----------------------
    // volume frame
    // ----------------------

    // create label
    this->volumeLabel = new QLabel("Volume OUT");

    //create lcd
    this->volumeLcd = new QLabel();
    this->volumeLcd->setNum(this->osci->volume());
    this->volumeLcd->setProperty("class","displayNumber");

    //create silder
    this->volumeSlider = new QSlider(Qt::Horizontal);
    this->volumeSlider->setProperty("class","volumeSlider");
    this->volumeSlider->setMaximum(100);
    this->volumeSlider->setMinimum(0);
    this->volumeSlider->setValue(this->osci->volume());

    QHBoxLayout *volumeLayout = new QHBoxLayout;
    volumeLayout->addWidget(this->volumeLabel);
    volumeLayout->addWidget(this->volumeSlider);
    volumeLayout->addWidget(this->volumeLcd);
    this->volumeFrame->setLayout(volumeLayout);

    //connection
    QObject::connect(this->volumeSlider, SIGNAL(valueChanged(int)), this->osci, SLOT(setVolume(int))) ;
    QObject::connect(this->osci, SIGNAL(volumeChanged(int)), this->volumeLcd, SLOT(setNum(int))) ;

    // ----------------------
    // oscillator frame
    // ----------------------

    //title
    this->oscillatorTitle = new QLabel("Oscillator");
    this->oscillatorTitle->setProperty("class","title");

    this->oscillatorPreview = new QLabel;
    this->oscillatorPreview->setPixmap(QPixmap(PATH+"/images/sine_preview.png"));

    this->oscillatorTypeLabel = new QLabel("Type");
    this->oscillatorTypeDisplay = new QLabel("Sine");
    this->oscillatorTypeDisplay->setProperty("class","typeDisplay");
    //BUTTON
    this->sineButton = new QRadioButton();
    this->triangleButton = new QRadioButton();
    this->squareButton = new QRadioButton();
    this->sawButton = new QRadioButton();
    this->sineButton->setProperty("class","sineButton");
    this->triangleButton->setProperty("class","triangleButton");
    this->squareButton->setProperty("class","squareButton");
    this->sawButton->setProperty("class","sawButton");
    this->sineButton->setChecked(true);
    this->oscillatorButtons = new QGroupBox();
    QHBoxLayout *oscillatorButtonsLayout = new QHBoxLayout();
    oscillatorButtonsLayout->setContentsMargins(0,0,0,0);
    oscillatorButtonsLayout->addWidget(this->sineButton);
    oscillatorButtonsLayout->addWidget(this->triangleButton);
    oscillatorButtonsLayout->addWidget(this->squareButton);
    oscillatorButtonsLayout->addWidget(this->sawButton);
    oscillatorButtonsLayout->addStretch(1);
    this->oscillatorButtons->setLayout(oscillatorButtonsLayout);
    QObject::connect(this->sineButton, SIGNAL(clicked(bool)), this->osci, SLOT(setSineVoice(bool))) ;
    QObject::connect(this->triangleButton, SIGNAL(clicked(bool)), this->osci, SLOT(setTriangleVoice(bool))) ;
    QObject::connect(this->squareButton, SIGNAL(clicked(bool)), this->osci, SLOT(setSquareVoice(bool))) ;
    QObject::connect(this->sawButton, SIGNAL(clicked(bool)), this->osci, SLOT(setSawVoice(bool))) ;
    QObject::connect(this->sineButton, SIGNAL(clicked(bool)), this, SLOT(setSineVoice(bool))) ;
    QObject::connect(this->triangleButton, SIGNAL(clicked(bool)), this, SLOT(setTriangleVoice(bool))) ;
    QObject::connect(this->squareButton, SIGNAL(clicked(bool)), this, SLOT(setSquareVoice(bool))) ;
    QObject::connect(this->sawButton, SIGNAL(clicked(bool)), this, SLOT(setSawVoice(bool))) ;

    // COARSE
    // create label
    this->coarseLabel = new QLabel("Coarse");
    //create lcd
    this->coarseDisplay = new QLabel();
    this->coarseDisplay->setProperty("class","displayNumber");
    this->coarseDisplay->setNum(0);
    //create silder
    this->coarseSlider = new QSlider(Qt::Horizontal);
    this->coarseSlider->setMaximum(12);
    this->coarseSlider->setMinimum(-12);
    this->coarseSlider->setValue(0);
    QObject::connect(this->coarseSlider, SIGNAL(valueChanged(int)), this->osci, SLOT(setCoarse(int))) ;
    QObject::connect(this->coarseSlider, SIGNAL(valueChanged(int)), this->coarseDisplay, SLOT(setNum(int))) ;

    // FINE
    // create label
    this->fineLabel = new QLabel("Fine");
    //create lcd
    this->fineDisplay = new QLabel();
    this->fineDisplay->setProperty("class","displayNumber");
    this->fineDisplay->setNum(0);
    //create silder
    this->fineSlider = new QSlider(Qt::Horizontal);
    this->fineSlider->setMaximum(100);
    this->fineSlider->setMinimum(-100);
    this->fineSlider->setValue(0);
    QObject::connect(this->fineSlider, SIGNAL(valueChanged(int)), this->osci, SLOT(setFine(int))) ;
    QObject::connect(this->fineSlider, SIGNAL(valueChanged(int)), this->fineDisplay, SLOT(setNum(int))) ;

    // UNISON
    // create label
    this->unisonLabel = new QLabel("Unison");
    //create lcd
    this->unisonDisplay = new QLabel();
    this->unisonDisplay->setProperty("class","displayNumber");
    this->unisonDisplay->setNum(0);
    //create silder
    this->unisonSlider = new QSlider(Qt::Horizontal);
    this->unisonSlider->setMaximum(100);
    this->unisonSlider->setMinimum(0);
    this->unisonSlider->setValue(0);
    this->unisonSlider->setEnabled(false);

    // DETUNE
    // create label
    this->detuneLabel = new QLabel("Detune");
    //create lcd
    this->detuneDisplay = new QLabel();
    this->detuneDisplay->setProperty("class","displayNumber");
    this->detuneDisplay->setNum(0);
    //create silder
    this->detuneSlider = new QSlider(Qt::Horizontal);
    this->detuneSlider->setMaximum(100);
    this->detuneSlider->setMinimum(0);
    this->detuneSlider->setValue(0);
    this->detuneSlider->setEnabled(false);

    QGridLayout *oscillatorLayout = new QGridLayout;
    oscillatorLayout->setSizeConstraint(QLayout::SetFixedSize);
    oscillatorLayout->addWidget(this->oscillatorTitle, 1, 1, 1 ,2);
    oscillatorLayout->addWidget(this->oscillatorTypeLabel, 2, 1);
    oscillatorLayout->addWidget(this->oscillatorTypeDisplay, 2, 2, 1, 2);
    oscillatorLayout->addWidget(this->oscillatorButtons, 2, 4);
    oscillatorLayout->addWidget(this->oscillatorPreview, 3,4,4,1);
    oscillatorLayout->addWidget(this->coarseLabel, 3,1);
    oscillatorLayout->addWidget(this->coarseSlider, 3,2);
    oscillatorLayout->addWidget(this->coarseDisplay, 3,3);
    oscillatorLayout->addWidget(this->fineLabel, 4,1);
    oscillatorLayout->addWidget(this->fineSlider, 4,2);
    oscillatorLayout->addWidget(this->fineDisplay, 4,3);
    oscillatorLayout->addWidget(this->unisonLabel, 5,1);
    oscillatorLayout->addWidget(this->unisonSlider, 5,2);
    oscillatorLayout->addWidget(this->unisonDisplay, 5,3);
    oscillatorLayout->addWidget(this->detuneLabel, 6,1);
    oscillatorLayout->addWidget(this->detuneSlider, 6,2);
    oscillatorLayout->addWidget(this->detuneDisplay, 6,3);
    this->oscillatorFrame->setLayout(oscillatorLayout);

    // ----------------------
    // filter frame
    // ----------------------
    this->filterTitle = new QLabel("Filter");
    this->filterTitle->setProperty("class","title");

    this->filterPreview = new QLabel;
    this->filterPreview->setPixmap(QPixmap(PATH+"/images/low_preview.png"));

    this->filterTypeLabel = new QLabel("Type");
    this->filterTypeDisplay = new QLabel("Low Pass");
    this->filterTypeDisplay->setProperty("class","typeDisplay");

    //BUTTON
    this->lowButton = new QRadioButton();
    this->highButton = new QRadioButton();
    this->bandButton = new QRadioButton();
    this->notchButton = new QRadioButton();
    this->lowButton->setProperty("class","lowButton");
    this->highButton->setProperty("class","highButton");
    this->bandButton->setProperty("class","bandButton");
    this->notchButton->setProperty("class","notchButton");
    this->lowButton->setChecked(true);
    this->filterButtons = new QGroupBox();
    QHBoxLayout *filterButtonsLayout = new QHBoxLayout();
    filterButtonsLayout->setContentsMargins(0,0,0,0);
    filterButtonsLayout->addWidget(this->lowButton);
    filterButtonsLayout->addWidget(this->highButton);
    filterButtonsLayout->addWidget(this->bandButton);
    filterButtonsLayout->addWidget(this->notchButton);
    filterButtonsLayout->addStretch(1);
    this->filterButtons->setLayout(filterButtonsLayout);
    QObject::connect(this->lowButton, SIGNAL(clicked(bool)), this->filter, SLOT(setLowFilter(bool))) ;
    QObject::connect(this->highButton, SIGNAL(clicked(bool)), this->filter, SLOT(setHighFilter(bool))) ;
    QObject::connect(this->bandButton, SIGNAL(clicked(bool)), this->filter, SLOT(setBandFilter(bool))) ;
    QObject::connect(this->notchButton, SIGNAL(clicked(bool)), this->filter, SLOT(setNotchFilter(bool))) ;
    QObject::connect(this->lowButton, SIGNAL(clicked(bool)), this, SLOT(setLowFilter(bool))) ;
    QObject::connect(this->highButton, SIGNAL(clicked(bool)), this, SLOT(setHighFilter(bool))) ;
    QObject::connect(this->bandButton, SIGNAL(clicked(bool)), this, SLOT(setBandFilter(bool))) ;
    QObject::connect(this->notchButton, SIGNAL(clicked(bool)), this, SLOT(setNotchFilter(bool))) ;

    // FREQUENCY
    // create label
    this->frequencyLabel = new QLabel("Frequency");
    //create lcd
    this->frequencyDisplay = new QLabel();
    this->frequencyDisplay->setProperty("class","displayNumber");
    this->frequencyDisplay->setNum(0);
    //create silder
    this->frequencySlider = new QSlider(Qt::Horizontal);
    this->frequencySlider->setMaximum(100);
    this->frequencySlider->setMinimum(0);
    this->frequencySlider->setValue(0);
    // Connect
    QObject::connect(this->frequencySlider, SIGNAL(valueChanged(int)), this->filter, SLOT(setFreq(int))) ;
    QObject::connect(this->frequencySlider, SIGNAL(valueChanged(int)), this->frequencyDisplay, SLOT(setNum(int))) ;

    // RESONANCE
    // create label
    this->resonanceLabel = new QLabel("Resonance");
    //create lcd
    this->resonanceDisplay = new QLabel();
    this->resonanceDisplay->setProperty("class","displayNumber");
    this->resonanceDisplay->setNum(0);
    //create silder
    this->resonanceSlider = new QSlider(Qt::Horizontal);
    this->resonanceSlider->setMaximum(100);
    this->resonanceSlider->setMinimum(0);
    this->resonanceSlider->setValue(32);
    QObject::connect(this->resonanceSlider, SIGNAL(valueChanged(int)), this->filter, SLOT(setReso(int))) ;
    QObject::connect(this->resonanceSlider, SIGNAL(valueChanged(int)), this->resonanceDisplay, SLOT(setNum(int))) ;

    QGridLayout *filterLayout = new QGridLayout;
    filterLayout->setSizeConstraint(QLayout::SetFixedSize);
    filterLayout->addWidget(this->filterTitle, 1, 1, 1 ,2);
    filterLayout->addWidget(this->filterTypeLabel, 2, 1);
    filterLayout->addWidget(this->filterTypeDisplay, 2, 2, 1, 2);
    filterLayout->addWidget(this->filterButtons, 2, 4);
    filterLayout->addWidget(this->frequencyLabel, 3, 1);
    filterLayout->addWidget(this->frequencySlider, 3, 2);
    filterLayout->addWidget(this->frequencyDisplay, 3, 3);
    filterLayout->addWidget(this->resonanceLabel, 4, 1);
    filterLayout->addWidget(this->resonanceSlider, 4, 2);
    filterLayout->addWidget(this->resonanceDisplay, 4, 3);
    filterLayout->addWidget(this->filterPreview, 3, 4, 2,1);
    this->filterFrame->setLayout(filterLayout);

    // ----------------------
    // envelope frame
    // ----------------------
    this->envelopeTitle = new QLabel("Envelopes", this->envelopeFrame);
    this->envelopeTitle->setProperty("class","title");

    this->envelopePreview = new QLabel;
    this->envelopePreview->setPixmap(QPixmap(PATH+"/images/envelope_preview.png"));

    this->envelope1Button = new QPushButton("Select");
    this->envelope1Button->setProperty("class","envelope1");

    // AMOUNT 1
    // create label
    this->amount1Label = new QLabel("Amount");
    this->amount1Label->setProperty("class","vertical");
    //create lcd
    this->amount1Display = new QLabel();
    this->amount1Display->setProperty("class","displayNumber");
    this->amount1Display->setNum(0);
    //create silder
    this->amount1Slider = new QSlider(Qt::Horizontal);
    this->amount1Slider->setMaximum(100);
    this->amount1Slider->setMinimum(0);
    this->amount1Slider->setValue(0);
    this->amount1Slider->setEnabled(false);

    this->envelope2Button = new QPushButton("Select");
    this->envelope2Button->setProperty("class","envelope2");

    // AMOUNT 2
    // create label
    this->amount2Label = new QLabel("Amount");
    this->amount2Label->setProperty("class","vertical");
    //create lcd
    this->amount2Display = new QLabel();
    this->amount2Display->setProperty("class","displayNumber");
    this->amount2Display->setNum(0);
    //create silder
    this->amount2Slider = new QSlider(Qt::Horizontal);
    this->amount2Slider->setMaximum(100);
    this->amount2Slider->setMinimum(0);
    this->amount2Slider->setValue(0);
    this->amount2Slider->setEnabled(false);

    // ATTACK 1
    // create label
    this->attack1Label = new QLabel("Attack");
    this->attack1Label->setProperty("class","vertical");
    //create lcd
    this->attack1Display = new QLabel();
    this->attack1Display->setProperty("class","displayNumber");
    this->attack1Display->setNum(0);
    //create silder
    this->attack1Slider = new QSlider(Qt::Vertical);
    this->attack1Slider->setMaximum(100);
    this->attack1Slider->setMinimum(0);
    this->attack1Slider->setValue(0);
    this->attack1Slider->setEnabled(false);

    // DECAY 1
    // create label
    this->decay1Label = new QLabel("Decay");
    this->decay1Label->setProperty("class","vertical");
    //create lcd
    this->decay1Display = new QLabel();
    this->decay1Display->setProperty("class","displayNumber");
    this->decay1Display->setNum(0);
    //create silder
    this->decay1Slider = new QSlider(Qt::Vertical);
    this->decay1Slider->setMaximum(100);
    this->decay1Slider->setMinimum(0);
    this->decay1Slider->setValue(0);
    this->decay1Slider->setEnabled(false);

    // SUSTAIN 1
    // create label
    this->sustain1Label = new QLabel("Sustain");
    this->sustain1Label->setProperty("class","vertical");
    //create lcd
    this->sustain1Display = new QLabel();
    this->sustain1Display->setProperty("class","displayNumber");
    this->sustain1Display->setNum(0);
    //create silder
    this->sustain1Slider = new QSlider(Qt::Vertical);
    this->sustain1Slider->setMaximum(100);
    this->sustain1Slider->setMinimum(0);
    this->sustain1Slider->setValue(0);
    this->sustain1Slider->setEnabled(false);

    // RELEASE 1
    // create label
    this->release1Label = new QLabel("Release");
    this->release1Label->setProperty("class","vertical");
    //create lcd
    this->release1Display = new QLabel();
    this->release1Display->setProperty("class","displayNumber");
    this->release1Display->setNum(0);
    //create silder
    this->release1Slider = new QSlider(Qt::Vertical);
    this->release1Slider->setMaximum(100);
    this->release1Slider->setMinimum(0);
    this->release1Slider->setValue(0);
    this->release1Slider->setEnabled(false);

    // ATTACK 2
    // create label
    this->attack2Label = new QLabel("Attack");
    this->attack2Label->setProperty("class","vertical");
    //create lcd
    this->attack2Display = new QLabel();
    this->attack2Display->setProperty("class","displayNumber");
    this->attack2Display->setNum(0);
    //create silder
    this->attack2Slider = new QSlider(Qt::Vertical);
    this->attack2Slider->setMaximum(100);
    this->attack2Slider->setMinimum(0);
    this->attack2Slider->setValue(0);
    this->attack2Slider->setEnabled(false);

    // DECAY 2
    // create label
    this->decay2Label = new QLabel("Decay");
    this->decay2Label->setProperty("class","vertical");
    //create lcd
    this->decay2Display = new QLabel();
    this->decay2Display->setProperty("class","displayNumber");
    this->decay2Display->setNum(0);
    //create silder
    this->decay2Slider = new QSlider(Qt::Vertical);
    this->decay2Slider->setMaximum(100);
    this->decay2Slider->setMinimum(0);
    this->decay2Slider->setValue(0);
    this->decay2Slider->setEnabled(false);

    // SUSTAIN 2
    // create label
    this->sustain2Label = new QLabel("Sustain");
    this->sustain2Label->setProperty("class","vertical");
    //create lcd
    this->sustain2Display = new QLabel();
    this->sustain2Display->setProperty("class","displayNumber");
    this->sustain2Display->setNum(0);
    //create silder
    this->sustain2Slider = new QSlider(Qt::Vertical);
    this->sustain2Slider->setMaximum(100);
    this->sustain2Slider->setMinimum(0);
    this->sustain2Slider->setValue(0);
    this->sustain2Slider->setEnabled(false);

    // RELEASE 2
    // create label
    this->release2Label = new QLabel("Release");
    this->release2Label->setProperty("class","vertical");
    //create lcd
    this->release2Display = new QLabel();
    this->release2Display->setProperty("class","displayNumber");
    this->release2Display->setNum(0);
    //create silder
    this->release2Slider = new QSlider(Qt::Vertical);
    this->release2Slider->setMaximum(100);
    this->release2Slider->setMinimum(0);
    this->release2Slider->setValue(0);
    this->release2Slider->setEnabled(false);

    QGridLayout *envelopeLayout = new QGridLayout;
    envelopeLayout->setSizeConstraint(QLayout::SetFixedSize);
    envelopeLayout->addWidget(this->envelopeTitle, 1, 1, 1 ,2);
    envelopeLayout->addWidget(this->envelopePreview, 2, 1, 1,4);
    envelopeLayout->addWidget(this->envelope1Button, 3, 1);
    envelopeLayout->addWidget(this->amount1Label, 3, 2);
    envelopeLayout->addWidget(this->amount1Slider, 3, 3);
    envelopeLayout->addWidget(this->amount1Display, 3, 4);

    envelopeLayout->addWidget(this->attack1Slider, 4, 1, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->attack1Display, 5, 1, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->attack1Label, 6, 1, Qt::AlignHCenter);

    envelopeLayout->addWidget(this->decay1Slider, 4, 2, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->decay1Display, 5, 2, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->decay1Label, 6, 2, Qt::AlignHCenter);

    envelopeLayout->addWidget(this->sustain1Slider, 4, 3, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->sustain1Display, 5, 3, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->sustain1Label, 6, 3, Qt::AlignHCenter);

    envelopeLayout->addWidget(this->release1Slider, 4, 4, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->release1Display, 5, 4, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->release1Label, 6, 4, Qt::AlignHCenter);

    envelopeLayout->addWidget(this->attack2Slider, 8, 1, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->attack2Display, 9, 1, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->attack2Label, 10, 1, Qt::AlignHCenter);

    envelopeLayout->addWidget(this->decay2Slider, 8, 2, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->decay2Display, 9, 2, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->decay2Label, 10, 2, Qt::AlignHCenter);

    envelopeLayout->addWidget(this->sustain2Slider, 8, 3, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->sustain2Display, 9, 3, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->sustain2Label, 10, 3, Qt::AlignHCenter);

    envelopeLayout->addWidget(this->release2Slider, 8, 4, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->release2Display, 9, 4, Qt::AlignHCenter);
    envelopeLayout->addWidget(this->release2Label, 10, 4, Qt::AlignHCenter);

    envelopeLayout->addWidget(this->envelope2Button, 7, 1);
    envelopeLayout->addWidget(this->amount2Label, 7, 2);
    envelopeLayout->addWidget(this->amount2Slider, 7, 3);
    envelopeLayout->addWidget(this->amount2Display, 7, 4);
    this->envelopeFrame->setLayout(envelopeLayout);

    // ----------------------
    // menu frame
    // ----------------------
    this->setLayout(windowLayout);
    QComboBox * combo = new QComboBox();
    QListView * listView = new QListView(combo);
    listView->setUniformItemSizes(true);
    listView->setMinimumWidth(400);
    listView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    listView->setBaseSize(QSize(400,24));
    combo->addItem("preset1");
    combo->addItem("preset2");
    combo->addItem("preset3");
    combo->addItem("preset4");
    combo->setView(listView);

    this->previousButton = new QPushButton();
    this->previousButton->setProperty("class","previous");
    this->nextButton = new QPushButton();
    this->nextButton->setProperty("class","next");
    this->folderButton = new QPushButton();
    this->folderButton->setProperty("class","folder");
    this->saveButton = new QPushButton();
    this->saveButton->setProperty("class","save");

    QHBoxLayout *menuLayout = new QHBoxLayout;
    menuLayout->setSpacing(0);
    menuLayout->setContentsMargins(0,0,0,0);
    menuLayout->addWidget(previousButton);
    menuLayout->addWidget(combo);
    menuLayout->addWidget(nextButton);
    menuLayout->addWidget(folderButton);
    menuLayout->addWidget(saveButton);
    this->menuFrame->setLayout(menuLayout);

}

Window::~Window()
{
}
