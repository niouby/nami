#ifndef BUFFER_H
#define BUFFER_H

#include <jack/jack.h>

#include "filter.h"
#include "oscillator.h"

#define KEY_NUMBER 128

using namespace std;

class Buffer
{
private:
    jack_default_audio_sample_t *leftChannel;
    jack_default_audio_sample_t *rightChannel;

    // For the sound engine.
    unsigned int index;
    bool keyState[KEY_NUMBER];
    unsigned int keyPressedNumber;

    Oscillator *oscillator;
    Filter     *filter;
public:
    Buffer(Oscillator &oscillator, Filter &filter);

    void branchOutputPort(jack_default_audio_sample_t *leftBuffer, jack_default_audio_sample_t *rightBuffer);

    void produce(void);

    void keyPressed(unsigned int key);
    void keyReleased(unsigned int key);
    void clearKeys(void);
};

#endif // BUFFER_H
