#ifndef WINDOW_H
#define WINDOW_H

#include <QApplication>
#include <QWidget>
#include <QCheckBox>
#include <QLCDNumber>
#include <QSlider>
#include <QFile>
#include <QFrame>
#include <QLabel>
#include <QLineEdit>
#include <QString>
#include <QGroupBox>
#include <QRadioButton>
#include <QHBoxLayout>
#include <QPixmap>
#include <QPushButton>
#include <QComboBox>
#include <QListView>

#include "filter.h"
#include "oscillator.h"

class Window : public QWidget
{
    Q_OBJECT
public:
    Window(Oscillator* o, Filter *filter);
    ~Window();

public slots:
    void setSineVoice(bool);
    void setTriangleVoice(bool);
    void setSquareVoice(bool);
    void setSawVoice(bool);
    void setLowFilter(bool);
    void setHighFilter(bool);
    void setBandFilter(bool);
    void setNotchFilter(bool);

private:
    Oscillator *osci;
    Filter *filter;

    // ----------------------
    // main window
    // ----------------------
    QWidget *logo;
    QFrame *logoFrame;
    QFrame *envelopeFrame;
    QFrame *filterFrame;
    QFrame *menuFrame;
    QFrame *oscillatorFrame;
    QFrame *volumeFrame;

    // ----------------------
    // oscillator panel
    // ----------------------
    QLabel *oscillatorTitle;
    QLabel *oscillatorPreview;

    QLabel *oscillatorTypeLabel;
    QLabel *oscillatorTypeDisplay;

    QRadioButton *sineButton;
    QRadioButton *triangleButton;
    QRadioButton *squareButton;
    QRadioButton *sawButton;
    QGroupBox *oscillatorButtons;

    QLabel *coarseDisplay;
    QSlider *coarseSlider;
    QLabel *coarseLabel;

    QLabel *fineDisplay;
    QSlider *fineSlider;
    QLabel *fineLabel;

    QLabel *unisonDisplay;
    QSlider *unisonSlider;
    QLabel *unisonLabel;

    QLabel *detuneDisplay;
    QSlider *detuneSlider;
    QLabel *detuneLabel;

    // ----------------------
    // volume panel
    // ----------------------
    QLabel *volumeLcd;
    QSlider *volumeSlider;
    QLabel *volumeLabel;

    // ----------------------
    // filter panel
    // ----------------------
    QLabel *filterTitle;
    QLabel *filterPreview;
    QLabel *filterTypeLabel;
    QLabel *filterTypeDisplay;

    QRadioButton *lowButton;
    QRadioButton *highButton;
    QRadioButton *bandButton;
    QRadioButton *notchButton;
    QGroupBox *filterButtons;

    QLabel *frequencyDisplay;
    QSlider *frequencySlider;
    QLabel *frequencyLabel;

    QLabel *resonanceDisplay;
    QSlider *resonanceSlider;
    QLabel *resonanceLabel;

    // ----------------------
    // envelope panel
    // ----------------------
    QLabel *envelopeTitle;
    QLabel *envelopePreview;

    QPushButton *envelope1Button;
    QLabel *amount1Display;
    QSlider *amount1Slider;
    QLabel *amount1Label;

    QPushButton *envelope2Button;
    QLabel *amount2Display;
    QSlider *amount2Slider;
    QLabel *amount2Label;

    QLabel *attack1Display;
    QSlider *attack1Slider;
    QLabel *attack1Label;

    QLabel *decay1Display;
    QSlider *decay1Slider;
    QLabel *decay1Label;

    QLabel *sustain1Display;
    QSlider *sustain1Slider;
    QLabel *sustain1Label;

    QLabel *release1Display;
    QSlider *release1Slider;
    QLabel *release1Label;

    QLabel *attack2Display;
    QSlider *attack2Slider;
    QLabel *attack2Label;

    QLabel *decay2Display;
    QSlider *decay2Slider;
    QLabel *decay2Label;

    QLabel *sustain2Display;
    QSlider *sustain2Slider;
    QLabel *sustain2Label;

    QLabel *release2Display;
    QSlider *release2Slider;
    QLabel *release2Label;

    QPushButton *previousButton;
    QPushButton *nextButton;
    QPushButton *folderButton;
    QPushButton *saveButton;
};


#endif // WINDOW_H
