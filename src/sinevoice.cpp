#include "sinevoice.h"

SineVoice::SineVoice(void)
{
}

jack_default_audio_sample_t SineVoice::generate(jack_default_audio_sample_t ramp)
{
    return sin(2 * M_PI * ramp);
}
