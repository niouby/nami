#ifndef CONSTANTS_H
#define CONSTANTS_H

// Colors.
#include <QColor>

QColor LIGHTRED(238,  58,  58);
QColor DARKRED( 65,   6,   6);
QColor LIGHTBLUE(138, 212, 189);
QColor DARKBLUE( 23,  64,  53);
QColor NAVYBLUE( 11,  30,  25);
QColor LIGHTGREEN( 13, 200,  29);
QColor DARKGREEN(  5,  73,   4);
QColor LIGHTBRUN( 52,  38,  31);
QColor DARKBRUN( 23,  17,  14);
QColor LIGHTPINK(248, 199, 173);

// GUI elements position.
// Main volume frame.
#define VOLUME_FRAME_X 16
#define VOLUME_FRAME_Y 536
#define VOLUME_FRAME_W 448
#define VOLUME_FRAME_H 40

// Oscillator frame.
#define OSCILLATOR_FRAME_X 16
#define OSCILLATOR_FRAME_Y 88
#define OSCILLATOR_FRAME_W 448
#define OSCILLATOR_FRAME_H 224

// Filter frame.
#define FILTER_FRAME_X 16
#define FILTER_FRAME_Y 320
#define FILTER_FRAME_W 448
#define FILTER_FRAME_H 208

// Envelope frame.
#define ENVELOPE_FRAME_X 472
#define ENVELOPE_FRAME_Y 88
#define ENVELOPE_FRAME_W 304
#define ENVELOPE_FRAME_H	488

// Menu frame.
#define MENU_FRAME_X 88
#define MENU_FRAME_Y 16
#define MENU_FRAME_W 688
#define MENU_FRAME_H 64

// path
#define PATH QCoreApplication::applicationDirPath() + "/../src/"

#endif // CONSTANTS_H
