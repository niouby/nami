#ifndef SQUAREVOICE_H
#define SQUAREVOICE_H

#include "voice.h"

class SquareVoice: public Voice
{
public:
    SquareVoice();

    jack_default_audio_sample_t generate(jack_default_audio_sample_t ramp);
};

#endif // SQUAREVOICE_H
