#include "filter.h"

Filter::Filter() {
    type = flt_type_lowpass;
    a0 = 1.0;
    a1 = a2 = b1 = b2 = 0.0;
    cutoffFrequency = 0.50;
    Q = 0.707;
    peakGain = 0.0;
    z1 = z2 = 0.0;
}

Filter::Filter(int type, float cutoffFrequency, float Q, float peakGainDB) {
    setFilter(type, cutoffFrequency, Q, peakGainDB);
    z1 = z2 = 0.0;
}

Filter::~Filter() {
}

void Filter::setType(int type) {
    this->type = type;
    calcFilter();
}

void Filter::setQ(float Q) {
    this->Q = Q;
    calcFilter();
}

void Filter::setcutoffFrequency(float cutoffFrequency) {
    this->cutoffFrequency = cutoffFrequency;
    calcFilter();
}

void Filter::setPeakGain(float peakGainDB) {
    this->peakGain = peakGainDB;
    calcFilter();
}

void Filter::setFilter(int type, float cutoffFrequency, float Q, float peakGainDB) {
    this->type = type;
    this->Q = Q;
    this->cutoffFrequency = cutoffFrequency;
    setPeakGain(peakGainDB);
}

void Filter::setLowFilter(bool) {
    this->setType(flt_type_lowpass);
}

void Filter::setHighFilter(bool) {
    this->setType(flt_type_highpass);
}

void Filter::setBandFilter(bool) {
    this->setType(flt_type_bandpass);
}

void Filter::setNotchFilter(bool) {
    this->setType(flt_type_notch);
}

void Filter::calcFilter(void) {
    float norm;
    float K = tan(M_PI * cutoffFrequency);
    switch (this->type) {
    case flt_type_lowpass:
        norm = 1 / (1 + K / Q + K * K);
        a0 = K * K * norm;
        a1 = 2 * a0;
        a2 = a0;
        b1 = 2 * (K * K - 1) * norm;
        b2 = (1 - K / Q + K * K) * norm;
        break;

    case flt_type_highpass:
        norm = 1 / (1 + K / Q + K * K);
        a0 = 1 * norm;
        a1 = -2 * a0;
        a2 = a0;
        b1 = 2 * (K * K - 1) * norm;
        b2 = (1 - K / Q + K * K) * norm;
        break;

    case flt_type_bandpass:
        norm = 1 / (1 + K / Q + K * K);
        a0 = K / Q * norm;
        a1 = 0;
        a2 = -a0;
        b1 = 2 * (K * K - 1) * norm;
        b2 = (1 - K / Q + K * K) * norm;
        break;

    case flt_type_notch:
        norm = 1 / (1 + K / Q + K * K);
        a0 = (1 + K * K) * norm;
        a1 = 2 * (K * K - 1) * norm;
        a2 = a0;
        b1 = a1;
        b2 = (1 - K / Q + K * K) * norm;
        break;
    }

    return;
}

float Filter::cutoffFreq() const {
    return this->m_cutoffFreq;
}

void Filter::setFreq(int f) {
    this->setcutoffFrequency((float(f) * 220.0 + 30.0) / 44100.0);
    emit this->freqChanged(f);
}

void Filter::setReso(int n) {
    this->setQ(n*0.019 + 0.1);
}
