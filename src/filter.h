/*
 * The code of this class comes from this website :
 * http://www.earlevel.com/main/2012/11/26/biquad-c-source-code/
 */

#ifndef FILTER_H
#define FILTER_H

#include <jack/jack.h>
#include <math.h>
#include <QObject>

enum {
    flt_type_lowpass = 0,
    flt_type_highpass,
    flt_type_bandpass,
    flt_type_notch,
};

class Filter : public QObject {
    Q_OBJECT
    Q_PROPERTY(float cutoffFreq READ cutoffFreq WRITE setFreq NOTIFY freqChanged)
public:
    float cutoffFreq() const;

    Filter();
    Filter(int type, float cutoffFrequency, float Q, float peakGainDB);
    ~Filter();
    void setType(int type);
    void setQ(float Q);
    void setcutoffFrequency(float cutoffFrequency);
    void setPeakGain(float peakGainDB);
    void setFilter(int type, float cutoffFrequency, float Q, float peakGain);
    jack_default_audio_sample_t process(jack_default_audio_sample_t in);

public slots:
    void setLowFilter(bool);
    void setHighFilter(bool);
    void setBandFilter(bool);
    void setNotchFilter(bool);
    void setFreq(int f);
    void setReso(int n);

signals:
    void freqChanged(float f);

protected:
    void calcFilter(void);

    int type;
    float a0, a1, a2, b1, b2;
    float cutoffFrequency, Q, peakGain;
    float z1, z2;

private:
    float m_cutoffFreq;
};

inline jack_default_audio_sample_t Filter::process(jack_default_audio_sample_t in) {
    jack_default_audio_sample_t out = in * a0 + z1;
    z1 = in * a1 + z2 - b1 * out;
    z2 = in * a2 - b2 * out;
    return out;
}

#endif // FILTER_H
