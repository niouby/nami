SOURCES += \
    main.cpp \
    window.cpp \
    oscillator.cpp \
    buffer.cpp \
    jackclient.cpp \
    voice.cpp \
    sinevoice.cpp \
    squarevoice.cpp \
    sawvoice.cpp \
    trianglevoice.cpp \
    filter.cpp

HEADERS += \
    window.h \
    oscillator.h \
    buffer.h \
    jackclient.h \
    voice.h \
    sinevoice.h \
    squarevoice.h \
    sawvoice.h \
    trianglevoice.h \
    filter.h \
    constants.h

QT += widgets

LIBS += -ljack

DISTFILES += \
    style.qss
