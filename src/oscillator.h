#ifndef OSCILLATOR_H
#define OSCILLATOR_H

#include <iostream>
#include <jack/jack.h>
#include <math.h>
#include <QObject>

#include "sawvoice.h"
#include "sinevoice.h"
#include "squarevoice.h"
#include "trianglevoice.h"
#include "voice.h"

#define KEY_NUMBER 128

#define UP   1.059463
#define DOWN 0.943874

enum {
    voice_type_sine = 0,
    voice_type_triangle,
    voice_type_square,
    voice_type_saw
};

class Oscillator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int volume READ volume WRITE setVolume NOTIFY volumeChanged)
public:
    Oscillator();
    int volume() const;

    jack_default_audio_sample_t createPoint(unsigned int key);
    void mute(unsigned int key);
public slots:
    void setVolume(int n);
    void setVoice(int v);
    void setSineVoice(bool);
    void setTriangleVoice(bool);
    void setSquareVoice(bool);
    void setSawVoice(bool);
    void setFine(int);
    void setCoarse(int);
signals:
    void volumeChanged(int n);
private:
    int m_volume;

    Voice *m_voice;

    jack_default_audio_sample_t detune;
    jack_default_audio_sample_t fine;

    jack_default_audio_sample_t ramp[KEY_NUMBER];
    jack_default_audio_sample_t notesFrequency[KEY_NUMBER];
};

#endif // OSCILLATOR_H
