#include <math.h>
#include <iostream>

#include "buffer.h"

using namespace std;

Buffer::Buffer(Oscillator &oscillator, Filter &filter)
{
    this->index = 0;

    // By default, all keys are unpressed.
    this->keyPressedNumber = 0;
    for (unsigned int i = 0; i < KEY_NUMBER; i++)
        this->keyState[i] = false;

    this->filter = &filter;
    this->oscillator = &oscillator;
}

void Buffer::branchOutputPort(jack_default_audio_sample_t *leftBuffer,
                              jack_default_audio_sample_t *rightBuffer)
{
    this->leftChannel  = leftBuffer;
    this->rightChannel = rightBuffer;
}

void Buffer::produce(void)
{
    jack_default_audio_sample_t point = 0.0;
    unsigned int index = this->index;

    // Create a waveform point for each key and filter the result.
    for (unsigned int i = 0; i < KEY_NUMBER; i++)
        if (this->keyState[i])
            point += this->filter->process(this->oscillator->createPoint(i));
        else
            this->oscillator->mute(i);

    // Put the point in the buffers.
    this->leftChannel[index]  = point;
    this->rightChannel[index] = point;

    // Increment the ramp.
    this->index = (++index == 256) ? 0 : index;
}

void Buffer::keyPressed(unsigned int key)
{
    // If the maximum number of pressed key is not reached, set the new key
    // pressed.
    if (this->keyPressedNumber < 8) {
        this->keyState[key] = true;
        this->keyPressedNumber++;
    }
}

void Buffer::keyReleased(unsigned int key)
{
    // Released the key.
    this->keyState[key] = false;

    // This condition prevent key count bugs.
    if (this->keyPressedNumber-- > 8)
        this->clearKeys();
}

void Buffer::clearKeys(void)
{
    this->keyPressedNumber = 0;
    for (unsigned int i = 0; i < KEY_NUMBER; i++)
        this->keyState[i] = false;
}
