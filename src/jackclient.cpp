#include "jackclient.h"

using namespace std;

JackClient::JackClient(Buffer &buffer)
{
    // Open the JACK client.
    jack_status_t status;
    if (!(this->client = jack_client_open("nami", JackNullOption, &status))) {
        cout << "Error: JACK server is not running ?" << endl;
        exit(EXIT_FAILURE);
    }
    if (status) {
        cout << "Error: problem with JACK (status = " << status << ")." << endl;
        exit(EXIT_FAILURE);
    }

    // Set the callbacks: process and shutdown.
    jack_set_process_callback(this->client, &JackClient::process, this);
    jack_on_shutdown(this->client, &JackClient::shutdown, NULL);

    // Set the ports.
    this->midiInputPort = jack_port_register(this->client, "midi_input", JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
    this->leftAudioOutputPort  = jack_port_register(this->client, "left_audio_output",  JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    this->rightAudioOutputPort = jack_port_register(this->client, "right_audio_output", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    if (!this->midiInputPort || !this->leftAudioOutputPort || !this->rightAudioOutputPort) {
        cout << "Not enough JACK ports available" << endl;
        exit(EXIT_FAILURE);
    }

    // Activate the JACK client.
    if (jack_activate(this->client)) {
        cout << "Error: cannot activate client." << endl;
        exit(EXIT_FAILURE);
    }

    this->buffer = &buffer;
}

int JackClient::process(jack_nframes_t nframes, void *arg)
{
    JackClient *self = (JackClient *)(arg);

    // Prepare the buffer.
    self->buffer->branchOutputPort((jack_default_audio_sample_t *)jack_port_get_buffer(self->leftAudioOutputPort,  nframes),
                                   (jack_default_audio_sample_t *)jack_port_get_buffer(self->rightAudioOutputPort, nframes));

    // Variables to retrieve events.
    void *midiBuffer = jack_port_get_buffer(self->midiInputPort, nframes);
    jack_midi_event_t inEvent;
    jack_nframes_t eventIndex = 0;
    jack_nframes_t eventCount = jack_midi_get_event_count(midiBuffer);

    // Tell the buffer if a MIDI event is recieved.
    jack_midi_event_get(&inEvent, midiBuffer, 0);
    for (unsigned int i = 0; i < nframes; i++) {
        while ((inEvent.time == i) && (eventIndex < eventCount)) {
            switch (*(inEvent.buffer) & 0xf0) {
            case 0xb0:
                switch (*(inEvent.buffer + 1)) {
                case 0x01:
                    cout << "Modwheel = " << (*(inEvent.buffer + 2)) << endl;
                    break;
                default:;
                    cout << "CC = " << +(*(inEvent.buffer + 1)) << endl;
                }
                break;
            case 0xf0:
                if (*(inEvent.buffer + 1) == 0)
                    self->buffer->clearKeys();
                break;
            case 0x90:
                self->buffer->keyPressed(*(inEvent.buffer + 1));
                break;
            case 0x80:
                self->buffer->keyReleased(*(inEvent.buffer + 1));
                break;
            default:
                ;
            }
            eventIndex++;
            if (eventIndex < eventCount)
                jack_midi_event_get(&inEvent, midiBuffer, eventIndex);
        }

        // Add a point to the buffer.
        self->buffer->produce();
    }

    return 0;
}

void JackClient::shutdown(void *arg)
{
    exit(EXIT_SUCCESS);
}
