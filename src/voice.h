#ifndef VOICE_H
#define VOICE_H

#include <jack/jack.h>
#include <math.h>

class Voice
{
public:
    Voice(void);

    virtual jack_default_audio_sample_t generate(jack_default_audio_sample_t ramp) = 0;
};

#endif // VOICE_H
