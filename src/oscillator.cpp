#include "oscillator.h"

using namespace std;

Oscillator::Oscillator()
{
    this->m_volume = 30;

    for (unsigned int i = 0; i < KEY_NUMBER; i++) {
        // From MIDI to frequency : f = 2 * 440 * 2^(d - 69 / 12) / fs.
        // The following line refers to a simplied version of this formula.
        this->notesFrequency[i] = 16.351598 * pow(2, i / 12.0) / 44100.0;
        this->ramp[i] = 0.0;
    }
    this->m_voice = new SineVoice();
    this->detune = 1.0;
    this->fine   = 0.0;
}

int Oscillator::volume() const
{
    return this->m_volume;
}

jack_default_audio_sample_t Oscillator::createPoint(unsigned int key)
{
    jack_default_audio_sample_t ramp;

    // Increment the ramp, i.e. the phase.
    ramp = this->ramp[key] + (this->notesFrequency[key] * (this->detune + this->fine));
    if (ramp >= 1.0)
        ramp -= 2.0;

    // Compute the point of the waveform and multiply it by the volume of
    // the sound.
    return (this->m_volume / 100.0) * this->m_voice->generate((this->ramp[key] = ramp));
}

void Oscillator::mute(unsigned int key)
{
    // Reset the phase.
    this->ramp[key] = 0.0;
}

void Oscillator::setVolume(int n)
{
    this->m_volume = n;
    emit this->volumeChanged(n);
}

void Oscillator::setSineVoice(bool)
{
    this->setVoice(voice_type_sine);
}

void Oscillator::setTriangleVoice(bool)
{
    this->setVoice(voice_type_triangle);
}

void Oscillator::setSquareVoice(bool)
{
    this->setVoice(voice_type_square);
}

void Oscillator::setSawVoice(bool)
{
    this->setVoice(voice_type_saw);
}

void Oscillator::setVoice(int v)
{
    Voice* tmp = this->m_voice;
    switch (v) {
    case voice_type_sine:
        this->m_voice = new SineVoice();
        break;
    case voice_type_triangle:
        this->m_voice = new TriangleVoice();
        break;
    case voice_type_square:
        this->m_voice = new SquareVoice();
        break;
    case voice_type_saw:
        this->m_voice = new SawVoice();
        break;
    }
    delete tmp;
}

void Oscillator::setFine(int n)
{
    this->fine = n * ((n >= 0) ? UP - 1.0 : 1.0 - DOWN ) / 100.0;
}

void Oscillator::setCoarse(int n)
{
    this->detune = pow ((n >= 0) ? UP : DOWN, abs(n));
}
