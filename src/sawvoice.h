#ifndef SAWVOICE_H
#define SAWVOICE_H

#include "voice.h"

class SawVoice: public Voice
{
public:
    SawVoice();

    jack_default_audio_sample_t generate(jack_default_audio_sample_t ramp);
};

#endif // SAWVOICE_H
