#ifndef SINEVOICE_H
#define SINEVOICE_H

#include "voice.h"

class SineVoice: public Voice
{
public:
    SineVoice(void);

    jack_default_audio_sample_t generate(jack_default_audio_sample_t ramp);
};

#endif // SINEVOICE_H
