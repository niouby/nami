#include <QApplication>
#include <QtWidgets>

#include "buffer.h"
#include "filter.h"
#include "oscillator.h"
#include "jackclient.h"
#include "window.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    Oscillator oscillator;
    Filter filter(flt_type_lowpass, 880.0 / 44100.0, 0.707, 0.0);
    Buffer buffer(oscillator, filter);
    JackClient jackClient(buffer);

    Window fenetre(&oscillator, &filter);
    fenetre.show();
    return app.exec();
}
