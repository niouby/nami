# Nami - virtual subtractive synthesizer

*Object oriented programming project for Polytech Marseille, year 4.*

---

## Introduction

Nami is a virtual subtractive synthesizer. It's an OOP project made for
Polytech Marseille in C++. Its purpose is to provide a simple synthesizer
which works with [JACK](http://www.jackaudio.org).

## Build

You will need *JACK 2* and *Qt Creator* (with *Qt 5.7*) to build Nami.
The *src.pro* file has to be loaded in *Qt Creator*, then you should be
able to build and use *Nami*.

## To do

### oscillator.cpp

* Replace `44100.0` by `jack_get_sample_rate(this->client)` in the constructor.

### buffer.cpp

* Replace `256` by `nframes` in the `Buffer::produce` method.
