#!/bin/sh

echo "MIDI connection"
jack_connect jack-keyboard:midi_out nami:midi_input
echo "Audio left"
jack_connect nami:left_audio_output system:playback_1
echo "Audio right"
jack_connect nami:right_audio_output system:playback_2
